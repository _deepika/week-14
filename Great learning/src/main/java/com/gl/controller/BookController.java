package com.gl.controller;

import com.gl.model.Books;
import com.gl.service.BooksRead;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Books")
public class BookController {

    @Autowired
    BooksRead boo;

    @PostMapping(path = "/book")
    public Books addBooks(@RequestBody Books book){
        return boo.addBooks(book);
    }

    @GetMapping(path = "/books")
    public List<Books> getAllBooks(){
        return boo.getAllBooks();
    }

    @GetMapping(path = "/book/{id}")
    public Books getBookById(@PathVariable("id") int id){
        return boo.getBooksById(id);
    }

    @GetMapping(path = "/books/name/{bookName}")
    public List<Books> getBookByName(@PathVariable("bookName") String bookName){
        return boo.getBooksByName(bookName);
    }

    @GetMapping(path = "/books/author/{author}")
    public List<Books> getBookByAuthorName(@PathVariable("author") String author){
        return boo.getBooksByAuthor(author);
    }

    @GetMapping(path = "/booksByAsc")
    public List<Books> getBooksByAscendingOrder(){
        return  boo.getBooksByAscendingOrder();
    }

    @GetMapping(path = "/booksByLast")
    public Books getBooksByLastAdded(){
        return boo.getBooksByLastAdded();
    }

    @DeleteMapping(path = "/deleteById")
    public void deleteById(@PathVariable("id") int id){
        boo.deleteById(id);
    }

    @DeleteMapping(path = "/deleteBooks")
    public void deleteAllBooks(){
        boo.deleteAllBooks();
    }


}
