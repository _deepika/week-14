package com.gl.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Books {

    @Id
    private int bookId;
    private String bookName;
    private String author;
    private float cost;

}




