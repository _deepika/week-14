package com.gl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.gl.model.Books;
import java.util.List;
@Repository
public interface BookRepository extends JpaRepository<Books, Integer> {
  
	
	List<Books> findByBookName(String bookName);
    List<Books> findByAuthor(String author);

}
